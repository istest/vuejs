import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);
import _ from "lodash";

export default new Vuex.Store({
  plugins: [createPersistedState()],
  state: {
    user: false,
    users: [{ login: "admin", pass: "1234" }, { login: "user", pass: "1234" }],
    usersList: []
  },
  getters: {
    users: state => {
      return state.users;
    },
    user: state => {
      return state.user;
    },
    usersList: state => {
      return state.usersList;
    },
    userById: state => id => {
      return _.find(state.usersList, { id: id });
    }
  },
  mutations: {
    authUser(state, user) {
      state.user = user;
    },
    logOut(state) {
      state.user = false;
    },
    addNewUser(state, userData) {
      state.usersList.push(userData);
    },
    editUser(state, userData) {
      _.remove(state.usersList, {
        id: userData.id
      });
      state.usersList.push(userData);
    },
    deleteUser(state, id) {
      let index = _.findIndex(state.usersList, {
        id: id
      });
      state.usersList.splice(index, 1);
    }
  },
  actions: {}
});
