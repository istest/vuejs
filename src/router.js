import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Login from "./views/Login.vue";
import Users from "./views/Users.vue";
import UsersForm from "./views/UsersForm.vue";
Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/login",
      name: "login",
      component: Login
    },
    {
      path: "/users",
      name: "users.list",
      component: Users
    },
    {
      path: "/users/add",
      name: "users.add",
      component: UsersForm
    },
    {
      path: "/users/:id",
      name: "users.edit",
      component: UsersForm
    }
  ]
});
